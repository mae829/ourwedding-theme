<?php
	/**
	 * The template for displaying 404 pages
	 */

	get_header();
?>

	<div <?php post_class(array('wrapper') ); ?>>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php the_title( '<h2>', '</h2>' ); ?>

			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. How on earth did you get here?!', 'sandm' ); ?></p>

		<?php endwhile; // End of the loop. ?>

	</div>

<?php get_footer();
