<?php
	/**
	 * The template for displaying all projects (Project Archive)
	 */

	get_header();

	$all_projects = new WP_Query( array(
		'post_type'			=> 'project',
		'post_status'		=> 'publish',
		'posts_per_page'	=> -1
	) );
?>

	<div id="work" class="wrapper archive project-archive">

		<h2>Work</h2>

		<?php if ( $all_projects->have_posts() ) : ?>

			<ul id="work-list">

				<?php while ( $all_projects->have_posts() ) : $all_projects->the_post(); ?>

					<li class="work-item">
						<a href="<?php the_permalink(); ?>">
							<span><?php the_title(); ?></span>
							<?php the_post_thumbnail( 'cropped-thumb' ); ?>
						</a>
					</li><!--END .WORK-ITEM-->

				<?php endwhile; ?>

			</ul><!--END #WORK-LIST-->

		<?php endif; // END IF OF THE WP LOOP ?>

	</div>

<?php get_footer();
