<?php
	/**
	 * Page template for the front page of the site
	 */
	get_header();

	if ( false === $pages_query = get_transient( 'pages_query' ) ) {

		$pages_args		= array(
			'post_type'		=> 'page',
			'order'			=> 'ASC',
			'orderby'		=> 'menu_order',
			'post_parent'	=> 0
		);

		$pages_query	= new WP_Query( $pages_args );

		set_transient( 'pages_query', $pages_query );
		echo '<!-- pages from new query -->';
	} else {
		echo '<!-- pages from cache -->';
	}

?>

	<div class="pages__wrapper col l5 offset-l7">

		<?php if ( $pages_query->have_posts() ) : while ( $pages_query->have_posts() ) : $pages_query->the_post();

			$template = get_post_meta( $post->ID, '_wp_page_template', true );

			if ( !$template || $template == 'default' ) {
				$template = 'page.php';
			}

			locate_template( $template, true, false );

		endwhile; else: ?>
			<p>Sorry, no content matched your criteria.</p>
		<?php endif; ?>

		<footer class="footer">

			<p>Powered by pizza &amp; purrs<img src="<?php echo get_template_directory_uri(); ?>/images/odin2-resized-compressor.png"  alt="Odin the Cat"></p>
		</footer>

	</div><!--END .pages__wrapper-->

<?php get_footer(); ?>
