<?php
	/**
	 * Template Name: RSVP
	 */
?>

			<div id="<?php echo $post->post_name; ?>" class="page-pane page-pane--<?php echo $post->post_name; ?>">

				<?php the_title( '<h2>', '</h2>' ); ?>
				<?php the_content(); ?>

				<form method="POST" class="form-horizontal invitees-search" action="" onsubmit="return false">
					<fieldset>

						<legend>Search for your name to RSVP</legend>

						<div class="row">

							<div class="input-field col s12">
								<input id="first_name" name="first_name" type="text" class="first_name">
								<label for="first_name">First Name</label>
							</div>
							<div class="input-field col s12">
								<input id="last_name" name="last_name" type="text" class="last_name">
								<label for="last_name">Last Name</label>
							</div>

						</div>

						<?php wp_nonce_field( 'invitee_search', 'search_nonce'); ?>

						<button type="submit" class="btn">Search</button>

					</fieldset>
				</form>

				<form method="POST" class="form-horizontal invitees-select hidden" action="" onsubmit="return false">
					<fieldset>

						<legend>Select your name</legend>

						<div class="invitees-found">

						</div>

						<button type="submit" class="btn">Select</button>

					</fieldset>
				</form>

				<form method="POST" class="form-horizontal invitees-attendance hidden" action="" onsubmit="return false">
					<fieldset>

						<legend>Will you be attending?</legend>

						<div class="row">
							<p>
								<input type="radio" name="invitee_response" id="invitee_response_yes" value="yes">
								<label for="invitee_response_yes">Yes! Wouldn't miss it for the world!</label>
							</p>

							<p>
								<input type="radio" name="invitee_response" id="invitee_response_no" value="no">
								<label for="invitee_response_no">Regretfully declines</label>
							</p>
						</div>

						<?php wp_nonce_field( 'invitee_rsvp', 'search_nonce' ); ?>

						<input type="hidden" name="invitee_id" value="">

						<button type="submit" class="btn">Submit</button>

					</fieldset>
				</form>

				<form method="POST" class="form-horizontal invitees-submission hidden" action="" onsubmit="return false">
					<fieldset>

						<legend>Please fill out details</legend>

						<div class="row">
							<div class="input-field col s12">
								<input type="text" name="invitee_name" id="invitee_name" value="null" disabled="disabled">
								<label for="invitee_name">Invitee Name</label>
							</div>
							<div class="col s12">
								<label for="food_choice">Your food choice</label>
								<select name="food_choice" id="food_choice" class="browser-default" required>
									<option value="" disabled selected>Please select</option>
									<option value="beef">Beef</option>
									<option value="fish">Fish</option>
								</select>
							</div>
							<div class="input-field col s12">
								<input type="email" name="email" id="email" value="" required>
								<label for="email">Your E-mail</label>
							</div>
							<div class="input-field col s12">
								<input type="text" name="song_choice" id="song_choice">
								<label for="song_choice">Your song request</label>
							</div>
							<p class="plusone col s12">
								<input type="checkbox" name="plusone_attending" id="plusone_attending">
								<label for="plusone_attending">Plus one attending?</label>
							</p>

							<div class="input-field plusone col s12">
								<input type="text" name="plusone_name" id="plusone_name" value="null" class="invitees-submission__plusone" disabled="disabled">
								<label for="plusone_name">Name of your plus one</label>
							</div>
							<div class="plusone col s12">
								<label for="plusone_food_choice">Plus one food choice:</label>
								<select name="plusone_food_choice" id="plusone_food_choice" class="browser-default">
									<option value="" disabled selected>Please select</option>
									<option value="beef">Beef</option>
									<option value="fish">Fish</option>
								</select>
							</div>
							<div class="input-field plusone col s12">
								<input type="text" name="plusone_song_choice" id="plusone_song_choice">
								<label for="plusone_song_choice">Plus one song request:</label>
							</div>

							<?php wp_nonce_field( 'invitee_submission', 'search_nonce'); ?>

							<input type="hidden" name="invitee-id" id="invitee-id" value="">
							<input type="hidden" name="plusone" id="plusone" value="">

							<button type="submit" class="btn">RSVP</button>

						</div>

					</fieldset>
				</form>

				<p class="invitees-message hidden"></p>

			</div>
