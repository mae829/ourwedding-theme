//=require vendor/jquery.2.1.0.min.js
//=require vendor/lozad.js
//=require vendor/materialize.js
//=require modules/functions.js
//=require modules/fonts.js
//=require modules/preloader.js
//=require modules/window-resize.js
//=require modules/menu.js
//=require modules/form.js
$(function() {

    // lazy loading
    var lazyElements    = document.querySelectorAll('.lazy-load');
    var observer        = lozad( lazyElements );

    observer.observe();

    // Materialize initialization
    $('select').material_select();
    $('.collapsible').collapsible();
    $('.modal').modal();

});
