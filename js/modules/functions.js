/*function debounce(func, wait, immediate) {
    wait = typeof wait !== 'undefined' ? wait : 20;
    immediate = typeof immediate !== 'undefined' ? immediate : true;
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}*/

function calculateElementVisibility( element ) {
    var elmentPosition = element.offsetTop,
        elementHeight  = $(element).height(), // have to use a little jQuery since the element might be floated
        aboveViewport  = window.scrollY - elmentPosition,
        belowViewport  = ( elmentPosition + elementHeight ) - ( window.scrollY + window.innerHeight );

    if ( ( window.scrollY > elmentPosition + elementHeight ) || ( elmentPosition > window.scrollY + window.innerHeight ) ) {
        return 0;
    } else {
        var result = 100;

        if ( aboveViewport > 0 ) {
            result -= (aboveViewport * 100) / elementHeight;
        }

        if ( belowViewport > 0 ) {
            result -= (belowViewport * 100) / elementHeight;
        }

        return result;
    }
}

