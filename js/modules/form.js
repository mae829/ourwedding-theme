$(function() {

    var $inviteesMessage    = $('.invitees-message');

    /**
     * Functionality for searching invitees
     */
    $('.invitees-search').on( 'submit', function(e) {
        e.preventDefault();

        // validate if at least one of the initial fields is submitted
        var $this           = $(this),
            $nonce          = $this.find('#search_nonce'),
            $firstName      = $this.find('.first_name'),
            $lastName       = $this.find('.last_name');

        // clear message in case there was already one
        $inviteesMessage.addClass('hidden').html('');

        if ( !$firstName.val() && !$lastName.val() ) {

            $firstName.addClass('invalid');
            $lastName.addClass('invalid');

            $inviteesMessage.html('You must submit at least one of the fields to search.').show();

        } else {

            /* Ajax submission code here */
            var ajaxData   = {
                'action': 'search_post_data',
                'search_nonce': $nonce.val(),
                'first_name': $firstName.val(),
                'last_name': $lastName.val()
            };

            $.ajax({
                url: search_post_data.ajax_url,
                type: 'POST',
                data: ajaxData,
                success: function( response ) {
                    // check that AJAX call return was successfully
                    if ( !response.success ) {
                        $inviteesMessage.html(response.data).show();
                    } else {

                        var $inviteesFound  = $('.invitees-found');

                        $.each( response.data, function( key, value ) {
                            var addInviteee = '<p><input type="radio" name="invitee" id="invitee'+ value.ID +'" data-invitee-id="'+ value.ID +'">';

                            addInviteee += ' <label for="invitee'+ value.ID +'">'+ value.first_name +' '+ value.last_name;

                            if ( parseInt( value.plusone ) ) {
                                addInviteee += ' &amp; '+ value.plusone_first_name +' '+ value.plusone_last_name;
                            }

                            addInviteee += '</label></p>';

                            $inviteesFound.append(addInviteee);

                        } );

                        $this.addClass('hidden');

                        $('.invitees-select').removeClass('hidden');

                    }
                },
                error: function( xhr, ajaxOptions, thrownError ){
                    $inviteesMessage.html('Error in AJAX initial call.').removeClass('hidden');
                    // console.log( xhr );
                }
            });

        }

        return false;

    } );

    /**
     * Functionality after invitee selection
     */
    $('.invitees-select').on( 'submit', function(e) {
        e.preventDefault();

        var $this   = $(this),
            $selectedInvitee    = $this.find('input[name=invitee]:checked'),
            inviteeID           = $selectedInvitee.data('invitee-id');

        // clear message in case there was already one
        $inviteesMessage.addClass('hidden').html('');

        if ( inviteeID === '' || isNaN( inviteeID ) ) {
            $inviteesMessage.html('Something went wrong with your choice. Please try submitting again.').removeClass('hidden');
        } else {

            $this.addClass('hidden');

            var $attendanceDiv = $('.invitees-attendance');

            $attendanceDiv.find('input[name=invitee_id]').val(inviteeID);

            $attendanceDiv.removeClass('hidden');

        }

        return false;

    } );

    /**
     * Functionalty after confirming attendance
     */
    $('.invitees-attendance').on( 'submit', function(e) {
        e.preventDefault();

        var $this               = $(this);

        var formData = $this.serializeArray().reduce( function( obj, item ) {
            obj[item.name] = item.value;
            return obj;
        }, {} );

        // clear message in case there was already one
        $inviteesMessage.addClass('hidden').html('');

        if ( formData.invitee_id === '' || isNaN( formData.invitee_id ) ) {
            $inviteesMessage.html('Something went wrong with your choice. Please try submitting again.').removeClass('hidden');
        } else {

            /* Ajax submission code here */
            var ajaxData   = {
                'action': 'search_post_data',
                'search_nonce': formData.search_nonce,
                'invitee_ID': formData.invitee_id,
                'response': formData.invitee_response
            };

            $.ajax({
                url: search_post_data.ajax_url,
                type: 'POST',
                data: ajaxData,
                success: function( response ) {
                    if ( !response.success || typeof response.data != 'object') {
                        // Hide old form
                        $this.addClass('hidden');

                        $inviteesMessage.html(response.data).removeClass('hidden');
                    } else {

                        var inviteeData         = response.data;
                        var $inviteesSubmssion  = $('.invitees-submission');

                        // Fill out form on page
                        $inviteesSubmssion.find('#invitee_name').val( inviteeData.first_name +' '+ inviteeData.last_name );
                        $inviteesSubmssion.find('#email').val( inviteeData.email );
                        $inviteesSubmssion.find('#invitee-id').val( inviteeData.ID );

                        // Check for plus one and fill out +1 info too
                        if ( !parseInt( inviteeData.plusone ) ) {
                            $inviteesSubmssion.find('.plusone').remove();
                        } else {
                            $inviteesSubmssion.find('#plusone_name').val( inviteeData.plusone_first_name +' '+ inviteeData.plusone_last_name );

                            $('#pluson[type="hidden]').val(1);

                            // Add check for plus one attending
                            var $plusoneAttending   = $inviteesSubmssion.find('#plusone_attending');
                            var $plusoneFoodChoice  = $inviteesSubmssion.find('#plusone_food_choice');

                            // $plusoneAttending.prop('required', true);

                            $plusoneAttending.on('click', function(){
                                if ( $plusoneAttending.is(':checked') ) {
                                    $plusoneFoodChoice.prop('required', true);
                                } else {
                                    $plusoneFoodChoice.prop('required', false);
                                }
                            });

                        }

                        // Hide old form
                        $this.addClass('hidden');

                        $inviteesSubmssion.removeClass('hidden');

                    }
                },
                error: function( xhr, ajaxOptions, thrownError ){
                    $inviteesMessage.html('Error in AJAX initial call.').removeClass('hidden');
                    // console.log( xhr );
                }
            });

        }

        return false;

    } );

    /**
     * Functionality for submitting the final data the invitee filled out
     */
    $('.invitees-submission').on( 'submit', function(e) {
        e.preventDefault();

        var $this           = $(this);

        var formData = $this.serializeArray().reduce( function( obj, item ) {
            obj[item.name] = item.value;
            return obj;
        }, {} );

        var $nonce          = formData.search_nonce;

        // clear message in case there was already one
        $inviteesMessage.addClass('hidden').html('');

        if ( formData['invitee-id'] === '' || isNaN( formData['invitee-id'] ) ) {
            $inviteesMessage.html('Something went wrong with your choice. Please try submitting again.').removeClass('hidden');
        } else {

            // Ajax submission code here
            var ajaxData   = {
                'action': 'search_post_data',
                'search_nonce': $nonce,
                'invitee_data': formData
            };

            $.ajax({
                url: search_post_data.ajax_url,
                type: 'POST',
                data: ajaxData,
                success: function( response ) {
                    if ( !response.success ) {
                        $inviteesMessage.html(response.data).show();
                    } else {

                        $this.addClass('hidden');

                        $inviteesMessage.html(response.data).addClass('strong').removeClass('hidden');

                    }
                },
                error: function( xhr, ajaxOptions, thrownError ){
                    $inviteesMessage.html('Error in AJAX initial call.').removeClass('hidden');
                    // console.log( xhr );
                }
            });

        }

        return false;

    } );

});
