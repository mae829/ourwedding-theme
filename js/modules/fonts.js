WebFontConfig = {
    classes: false,
    events: false,
    google: {
        families: ['Raleway:400,500']
    },
    custom: {
        families: ['sheraton']
    }
};

(function(d) {
    var wf = d.createElement('script'),
        s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
    wf.async = true;
    s.parentNode.insertBefore(wf, s);
})(document);
