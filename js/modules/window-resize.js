var $slider = $('.slider'),
    sliderOpts  = {
        transition: 2000,
        interval: 2000

    },
    $activeSlider;

if ( window.innerWidth < 900 ) {
    $activeSlider = $slider.slider(sliderOpts);
    $slider.removeClass('off');
}

function bodyClass() {

    if ( window.innerWidth < 900 ) {

        if ( $('.slider').hasClass('off') ) {
            $slider.removeClass('off');
            $activeSlider = $slider.slider(sliderOpts);
        }

        document.querySelector('body').classList.add('small-screen');
    } else {

        if ( !$slider.hasClass('off') ) {
            $slider.addClass('off');
            $activeSlider.slider('destroy');
        }

        document.querySelector('body').classList.remove('small-screen');
    }
}

bodyClass();
window.addEventListener( 'resize', bodyClass );
