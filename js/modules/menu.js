$(function() {

    var $menuLinks  = $('.side-nav a'),
        $content    = $('.content'),
        $body       = $('body'),
        $hamburger  = $('.hamburger'),
        $bgPane     = $('.backgrounds-pane');

    var pages = document.querySelectorAll('.page-pane');

    window.addEventListener( 'scroll', function() {
        clearTimeout( $.data( this, 'scrollCheck' ) );
        $.data( this, 'scrollCheck', setTimeout(function() {
            theMagic();
        }, 250) );
    } );

    // Hamburger menu is set up for Materialize
    $hamburger.sideNav({
        menuWidth: 180,
        edge: 'right',
        closeOnClick: true,
        onOpen: toggleMenu,
        onClose: toggleMenu
    });

    // scroll to that proper page clicked in the menu
    $menuLinks.each( function( index ){
        var $this   = $(this),
            page    = this.hash == '' ? '#home' : this.hash;

        $this.on( 'click', function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $(page).offset().top
            }, 750 ).promise().then( theMagic );

        } );

    });

    theMagic();

    function theMagic() {

        /* Do the menu change */
        // Get the percent each element is on screen (only two at a time)
        var pagesPercents    = {};

        // ES6 but gotta support IE
        /*pages.forEach( function( page ) {
            pagesPercents[page.getAttribute('id')] = calculateElementVisibility( page );
        } );*/

        // jQuery method
        /*$(pages).each( function( i, page ) {
            pagesPercents[page.getAttribute('id')] = calculateElementVisibility( page );
        } );*/

        // Standard JS
        for ( var i = 0; i < pages.length; ++i ) {
            pagesPercents[pages[i].getAttribute('id')] = calculateElementVisibility( pages[i] );
        }

        // Decide which element has the most percent on screen
        var activePageID = Object.keys( pagesPercents ).reduce( function(a, b){ return pagesPercents[a] > pagesPercents[b] ? a : b; } );

        $menuLinks.each( function() {
            var $this       = $(this),
                linkID      = $this.attr('href').slice(1);
                isActive    = $this.hasClass('current-menu-item');

            if ( !isActive && linkID === activePageID ) {
                $menuLinks.removeClass('current-menu-item');
                $this.addClass('current-menu-item');
            }
        });

        // fade in image on main pane and add a class to identify it through CSS
        if ( !$body.hasClass('small-screen') ) {

            $body
                .removeClass( function( index, className) {
                    return ( className.match (/(^|\s)active-page-\S+/g) || [] ).join(' ');
                } )
                .addClass('active-page-'+ activePageID );

            var $activeimg  = $bgPane.find('.backgrounds-pane__bgs .active'),
                $nextimg    = $bgPane.find('.backgrounds-pane__bgs-single.'+ activePageID);

            if ( !$nextimg.hasClass('active') ) {

                $activeimg.css( 'z-index', 2 );

                $nextimg
                    .css( 'z-index', 3 )
                    .addClass('active')
                    .fadeIn( 1000, function() {

                        $activeimg
                            .removeClass('active')
                            .css( 'z-index', 1 )
                            .fadeOut( 200 );

                    } );

            }

        }

    }

    function toggleMenu(){
        $content.toggleClass('open');
        $body.toggleClass('no-drag');
        $hamburger.toggleClass('open');
    }

});
