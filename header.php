<!DOCTYPE html>
	<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> <html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js"> <!--<![endif]-->
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Hi :) - Mikey -->
		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>
		<!--[if lte IE 9]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->

		<!-- quick way to load js class to html tag -->
		<script>
			// duh since this is running obviously JS is on
			var htmlTag	= document.querySelector('html');
			htmlTag.classList.remove('no-js');
			htmlTag.classList.add('js');

			document.querySelector('body').classList.add('pre-load-active');
		</script>

		<div class="pre-loader">
			<div class="sk-folding-cube">
			  <div class="sk-cube1 sk-cube"></div>
			  <div class="sk-cube2 sk-cube"></div>
			  <div class="sk-cube4 sk-cube"></div>
			  <div class="sk-cube3 sk-cube"></div>
			</div>
		</div>

		<div class="hamburger" data-activates="slide-out">
			<span></span>
			<span></span>
			<span></span>
		</div>

		<ul id="slide-out" class="menu side-nav right-aligned">
			<?php

				if ( false === $menu_pages = get_transient( 'menu_pages' ) ) {

					$menu_items = '';

					$args = array(
						'sort_order'	=> 'asc',
						'sort_column'	=> 'menu_order',
					);

					$menu_pages = get_pages($args);

					foreach ( $menu_pages as $key => $page ) {
						$menu_pages[$key]->url	= get_page_link( $page->ID );
					}

					set_transient( 'menu_pages', $menu_pages );
					echo "<!-- menu from new query -->\n\t\t\t";

				} else {
					echo "<!-- menu from cache -->\n\t\t\t";
				}

				foreach ( $menu_pages as $key => $page ) {
					$url			= str_replace( home_url(), '', $page->url );
					$url			= str_replace( '/', '', $url );
					$li_classes		= array('menu-item');
					$current_class	= '';

					if ( $key === 0 ) {
						$current_class	= ' class="current-menu-item"';
						$url	= 'home';
					}

					echo '<li class="'. implode( ' ', $li_classes ) .'"><a href="#' . $url . '"'. $current_class .'>' . $page->post_title . '</a></li>'."\n\t\t\t";
				}
			?>

		</ul>

		<div class="row">

			<div class="backgrounds-pane col s12 l7">

				<h1><a href="<?php echo site_url(); ?>" data-text="Savannah &amp; Mikey">Savannah &amp; Mikey</a></h1>

				<?php
					// Set up all the images for this section so that the fade works well
					$pages_query	= new WP_Query( array(
						'post_type'		=> 'page',
						'order'			=> 'ASC',
						'orderby'		=> 'menu_order',
						'post_status'	=> array( 'publish', 'draft' )
					) );

					$backgrounds_pane	= '';
					$slider				= '';

					if ( $pages_query->have_posts() ) :

					$backgrounds_pane	.= '<div class="backgrounds-pane__bgs">';
					$slider				.= '<div class="slider fullscreen off"><ul class="slides">';

					while ( $pages_query->have_posts() ) : $pages_query->the_post();
						if ( $post->post_status == 'publish' ) :
						$backgrounds_pane .= '<div class="backgrounds-pane__bgs-single '. $post->post_name . ( $pages_query->current_post == 0 ? ' active' : '') .'"'. ( has_post_thumbnail() ? ' style="background-image: url('. get_the_post_thumbnail_url( null, 'full' ) .')' : '') .'"></div>';
						endif;
						$slider .= '<li><img src="'. get_the_post_thumbnail_url( null, 'full' ) .'" alt="Savannah &amp; Mikey"></li>';
					endwhile;

					$backgrounds_pane	.= '</div>';
					$slider				.= '</ul></div>';

					endif;

					wp_reset_query();

					echo $backgrounds_pane;
					echo $slider;
				?>

			</div>
