var browserSync     = require('browser-sync').create(),
    gulp            = require('gulp'),
    autoprefixer    = require('gulp-autoprefixer'),
    cleanCSS        = require('gulp-clean-css'),
    concat          = require('gulp-concat'),
    include         = require('gulp-include'),
    jshint          = require('gulp-jshint'),
    plumber         = require('gulp-plumber'),
    rename          = require('gulp-rename'),
    // replace         = require('gulp-replace'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify');

var onError = function (err) {
    console.log('An error occurred:', err.message);
    this.emit('end');
};

gulp.task( 'css', function () {
    return gulp.src('css/sass/main.scss')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(rename({
            basename: 'style',
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
});

gulp.task( 'lint', function() {
    return gulp.src([
        'js/modules/*.js',
        'js/main.js'
    ])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
} );

gulp.task('scripts', function() {
    return gulp.src('js/main.js')
        .pipe(plumber({ errorHandler: onError }))
        .pipe(include())
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('./js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./js'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('browser-sync', function() {
    browserSync.init({
        files: ['./**/(*.html|*.php|*.min.css|*.min.js)', '!./**/(*.map)'],
        proxy: 'savannahandmikey.loc',
        tunnel: 'savannahandmikey', // for use if not on same wifi
        // online: false,
        open: false,
        // browser: ['firefox']
        // logLevel: 'debug'
    });

    gulp.watch('css/**/*.scss', ['css']);
    gulp.watch(['js/**/*.js', '!js/**/*.min.js'], ['lint', 'scripts']);

});

// Default Task
gulp.task('default', ['browser-sync']);

// run default task for compiling all files
gulp.task('compile', ['scripts', 'css']);
