<?php
	/**
	 * Define Theme Version (in style.css)
	 */
	$themeinfo = wp_get_theme();
	define( 'SM_VER', $themeinfo->get( 'Version') );

	// $stuff	= get_option('theme_mods_sm');
	// write_to_log($stuff);

	/**
	 * Require files
	 */
	// Theme
	require_once 'inc/admin-setup.php';
	require_once 'inc/theme-setup.php';
	require_once 'inc/invitees.php';

	// Menus/Navigation
	// require_once 'inc/theme-menus.php';

	// Mods & Enhancements
	require_once 'inc/shortcodes.php';
	// require_once( 'inc/comments.php' );


	/**
	 * Initiate classes
	 */
	// Theme
	Admin_Setup::singleton();
	Theme_Setup::singleton();
	Invitees::singleton();
	// Theme_Menus::singleton();
	Shortcodes::singleton();

	add_action( 'after_setup_theme', array( 'Theme_Setup', 'do_after_theme_setup' ) );


	/**
	 * Get Meta
	 *
	 * Utility function used to consolidate the querying of multiple meta values
	 * for the given object.
	 *
	 * @param int	 	$id ID of the current object.
	 * @param mixed		$fields Array/string containing meta field(s) to retrieve from database.
	 * @param string	$type Type of metadata request. Options: post/term/user
	 * @param constant	$output pre-defined constant for return type (OBJECT/ARRAY_A)
	 *
	 * @return mixed	MySQL object/Associative Array containing returned post metadata.
	 */
	function get_meta( $id = null, $fields = array(), $type = 'post', $output = ARRAY_A ) {
		global $wpdb;

		$fields		= esc_sql( $fields );
		$values_arr	= array();
		$values_obj	= new stdClass();
		$dbtable	= $wpdb->{$type.'meta'};
		$column_id	= $type . '_id';
		$id			= $id == null ? get_the_ID() : $id ;

		// make sure ID exists or query will return error. or else return NULL
		if ( is_null( $id ) || $id === false ) {
			return null;
		}

		$query		= "SELECT meta_key, meta_value FROM {$dbtable} WHERE {$column_id} = {$id}";

		if ( !empty( $fields ) ) {

			if ( is_array( $fields ) ) {
				$query .= " AND meta_key IN ('". implode("','", $fields) ."')";
			} else {
				$query .= " AND meta_key = '{$fields}'";
			}
		}

		$results	=  $wpdb->get_results( $query, OBJECT_K );

		foreach ( $results as $key => $result ) {
			$values_arr[$key]	= is_serialized( $result->meta_value ) ? unserialize( $result->meta_value ) : $result->meta_value;
			$values_obj->{$key}	= is_serialized( $result->meta_value ) ? unserialize( $result->meta_value ) : $result->meta_value;
		}

		if ( !is_array( $fields ) && !empty( $values_arr[$fields] ) ) {

			return $output == ARRAY_A ? $values_arr[$fields] : $values_obj[$fields];

		}

		return $output == ARRAY_A ? $values_arr : $values_obj;
	}

	/**
	 * Has Gallery
	 *
	 * Detect whether the page/post has a gallery.
	 *
	 * @return boolean True if page/post content has a gallery, else false.
	 */
	function has_gallery() {

		global $post;

		if ( !empty( $post ) && is_singular() ) {

			$content	= $post->post_content;

			if ( has_shortcode( $content, 'gallery' ) ) {

				return true;

			}

		}

		return false;

	}
