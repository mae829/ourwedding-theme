			<div id="<?php echo $post->post_name; ?>" class="page-pane page-pane--<?php echo $post->post_name; ?>">

				<?php
					if ( 'Home' !== get_the_title() ) {
						the_title( '<h2>', '</h2>' );
					}

					the_content();
				?>

			</div>
