<?php

class Admin_Setup {

	public static $instance = false;

	public function __construct() {
		$this->_setup_hooks();
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Setup Hooks
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _setup_hooks() {

		// back-end actions
		add_action( 'manage_posts_custom_column', array( $this, 'id_column_content' ), 5, 2 );
		add_action( 'manage_pages_custom_column', array( $this, 'id_column_content' ), 5, 2 );
		add_action( 'manage_category_custom_column', array( $this, 'id_column_content_term' ), 10, 3 );
		add_action( 'manage_post_tag_custom_column', array( $this, 'id_column_content_term' ), 10, 3 );
		add_action( 'post-plupload-upload-ui', array( $this, 'upload_suggestions_message' ) );
		add_action( 'post-html-upload-ui', array( $this, 'upload_suggestions_message' ) );
		add_action( 'save_post', array( $this, 'do_save_post' ), 10, 3 );


		// back-end filters
		// add_filter( 'wp_handle_upload_prefilter', array( $this, 'limit_image_size' ) );
		add_filter( 'manage_posts_columns', array( $this, 'add_id_column' ), 5 );
		add_filter( 'manage_pages_columns', array( $this, 'add_id_column' ), 5 );
		add_filter( 'manage_edit-category_columns', array( $this, 'add_id_column' ), 5 );
		add_filter( 'manage_edit-post_tag_columns', array( $this, 'add_id_column' ), 5 );
		add_filter( 'media_view_settings', array( $this, 'default_type_set_link' ) );

		add_filter( 'manage_pages_columns', array( $this, 'my_columns' ) );
		add_action( 'manage_pages_custom_column', array( $this, 'my_show_columns' ) );

	}

	/*
	 * Displays the ID for non-term in admin area columns
	 */
	public function id_column_content( $column, $id ) {

		if ( 'id' == $column ) {
			echo $id;
		}

	}

	/*
	 * Displays the ID for terms in admin area columns
	 */
	public function id_column_content_term( $value, $column, $cat_id ) {

		if ( 'id' == $column ) {
			echo $cat_id;
		}

	}

	public function upload_suggestions_message() {
		?>
		<h3>Uploading Files Guide</h3>
		<p class="upload-html-bypass hide-if-no-js">Here are a few options to reduce the size of images/files before uploading them (please always do this to help load the site faster for visitors!!).</p>
		<ul>
			<li><a href="https://compressor.io/" target="_blank" rel="noopener">Compressor.io - Compress all your images with this site.</a></li>
			<li><a href="https://smallpdf.com/" target="_blank" rel="noopener">SmallPDF.com - Compress those PDFs as well, for clients or on page display.</a></li>
		</ul>
		<?php
	}

	/**
	 * Do Save Post
	 *
	 * @param int $post_id ID of the updated post
	 * @param WP_Post $post WP_Post object containing the current post
	 */
	public function do_save_post( $post_id = null, $post = null, $update = null ) {

		// if this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
			return;
		}

		// make sure the post_type is "page"
		if ( isset( $_POST['post_type'] ) && 'page' != $_POST['post_type'] ) {
			return;
		}

		// make sure the post_status is "publish"
		if ( isset( $_POST['post_status'] ) && 'publish' != $_POST['post_status'] ) {
			return;
		}

		// get the post categories
		$post_categories = get_the_category( $post_id );
		$cats = array();

		if( ! empty( $post_categories ) ) {

			foreach( $post_categories as $post_category ) {
				$cats[] = $post_category->term_id;
			}

		}

		// remove our posts transient
		delete_transient( 'pages_query' );
		delete_transient( 'menu_pages' );

	}

	/**
	 * Limits the dimensions of an uploaded image
	 *
	 * @param  array $file Information about the attempted uploaded file
	 * @return array       Contains the information of the uploaded file or an error message with the data
	 */
	public function limit_image_size( $file ) {

		$image		= getimagesize( $file['tmp_name'] );
		$maximum	= array(
			'width'		=> '2000',
			'height'	=> '3000'
		);
		$image_width	= $image[0];
		$image_height	= $image[1];

		if ( $image_width > $maximum['width'] || $image_height > $maximum['height'] ) {
			$file['error']	= "Image dimensions are too large. Maximum size is {$maximum['width']} by {$maximum['height']} pixels. Uploaded image is $image_width by $image_height pixels.";
			return $file;
		} else{
			return $file;
		}

	}

	/*
	 * Add the ID column to an admin page
	 */
	public function add_id_column( $columns ) {
		global $post;

		// array of post types we don't want the column in
		$unwanted_cpts	= array('advert', 'advert-bidder');

		if ( isset($post) && !in_array( $post->post_type, $unwanted_cpts ) ) {
			$columns['id'] = 'ID';
		}

		return $columns;

	}

	/**
	 * Set the gallery settings default so that it links to the file
	 * instead of attachment page
	 */

	public function default_type_set_link( $settings ) {

		$settings['galleryDefaults']['link'] = 'file';
		return $settings;

	}

	public function my_columns( $columns ) {
		$columns['order'] = 'Order';
		return $columns;
	}

	public function my_show_columns( $name ) {
		global $post;

		switch ($name) {
			case 'order':
				$views = $post->menu_order;
				echo $views;
				break;
		}
	}
}
