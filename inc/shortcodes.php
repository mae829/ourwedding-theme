<?php
	class Shortcodes {

		public static $instance = false;

		public function __construct() {
			$this->_setup_hooks();
		}

		/**
		 * Singleton
		 *
		 * Returns a single instance of the current class.
		 */
		public static function singleton() {

			if ( !self::$instance )
				self::$instance = new self();

			return self::$instance;

		}


		/**
		 * Set up hooks
		 *
		 * Defines all the WordPress actions and filters used by this class.
		 */
		protected function _setup_hooks() {

			add_shortcode( 'gallery', array( $this, 'gallery_embed' ) );
			add_shortcode( 'project_gallery', array( $this, 'project_gallery_embed' ) );

		}


		/**
		 * Gallery shortcode mainly a copy of the native WP gallery shortcode
		 */
		public function gallery_embed( $attr ) {
			$post = get_post();

			static $instance = 0;
			$instance++;

			if ( ! empty( $attr['ids'] ) ) {
				// 'ids' is explicitly ordered, unless you specify otherwise.
				if ( empty( $attr['orderby'] ) ) {
					$attr['orderby'] = 'post__in';
				}
				$attr['include'] = $attr['ids'];
			}

			$html5 = current_theme_supports( 'html5', 'gallery' );
			$atts = shortcode_atts( array(
				'order'      => 'ASC',
				'orderby'    => 'menu_order ID',
				'id'         => $post ? $post->ID : 0,
				'itemtag'    => $html5 ? 'figure'     : 'dl',
				'icontag'    => $html5 ? 'div'        : 'dt',
				'captiontag' => $html5 ? 'figcaption' : 'dd',
				'columns'    => 3,
				'size'       => 'thumbnail',
				'include'    => '',
				'exclude'    => '',
				'link'       => ''
			), $attr, 'gallery' );

			$id = intval( $atts['id'] );

			if ( ! empty( $atts['include'] ) ) {
				$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

				$attachments = array();
				foreach ( $_attachments as $key => $val ) {
					$attachments[$val->ID] = $_attachments[$key];
				}
			} elseif ( ! empty( $atts['exclude'] ) ) {
				$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
			} else {
				$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
			}

			if ( empty( $attachments ) ) {
				return '';
			}

			if ( is_feed() ) {
				$output = "\n";
				foreach ( $attachments as $att_id => $attachment ) {
					$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
				}
				return $output;
			}

			$itemtag = tag_escape( $atts['itemtag'] );
			$captiontag = tag_escape( $atts['captiontag'] );
			$icontag = tag_escape( $atts['icontag'] );
			$valid_tags = wp_kses_allowed_html( 'post' );
			if ( ! isset( $valid_tags[ $itemtag ] ) ) {
				$itemtag = 'dl';
			}
			if ( ! isset( $valid_tags[ $captiontag ] ) ) {
				$captiontag = 'dd';
			}
			if ( ! isset( $valid_tags[ $icontag ] ) ) {
				$icontag = 'dt';
			}

			$columns = intval( $atts['columns'] );
			$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
			$float = is_rtl() ? 'right' : 'left';

			$selector = "gallery-{$instance}";

			$size_class = sanitize_html_class( $atts['size'] );
			$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

			$output = $gallery_div;

			$i = 0;
			foreach ( $attachments as $id => $attachment ) {

				$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
				if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
					$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
				} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
					$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
				} else {
					$image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
				}
				$image_meta  = wp_get_attachment_metadata( $id );

				// if the image is a gif, we want the markup for a ribbon to highlight to the user
				if ( substr_compare( $image_meta['file'], '.gif', -4 ) === 0 ) {
					$image_output	= str_replace( '<img ', '<div class="ribbon"><div class="text">GIF</div></div><img ', $image_output );
				}

				// add fancybox to gallery
				$image_output	= str_replace( '<a href=', '<a data-fancybox="gallery" href=', $image_output );

				$orientation = '';
				if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
					$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
				}
				$output .= "<{$itemtag} class='gallery-item'>";
				$output .= "
					<{$icontag} class='gallery-icon {$orientation}'>
						$image_output
					</{$icontag}>";
				if ( $captiontag && trim($attachment->post_excerpt) ) {
					$output .= "
						<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
						" . wptexturize($attachment->post_excerpt) . "
						</{$captiontag}>";
				}
				$output .= "</{$itemtag}>";
				if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
					$output .= '<br style="clear: both" />';
				}
			}

			if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
				$output .= "
					<br style='clear: both' />";
			}

			$output .= "
				</div>\n";

			return $output;
		}


		/**
		 * Project gallery shortcode mainly a copy of the native WP gallery shortcode
		 */
		public function project_gallery_embed( $attr ) {
			$post = get_post();

			static $instance = 0;
			$instance++;

			if ( ! empty( $attr['ids'] ) ) {
				// 'ids' is explicitly ordered, unless you specify otherwise.
				if ( empty( $attr['orderby'] ) ) {
					$attr['orderby'] = 'post__in';
				}
				$attr['include'] = $attr['ids'];
			}

			$html5 = current_theme_supports( 'html5', 'gallery' );
			$atts = shortcode_atts( array(
				'order'      => 'ASC',
				'orderby'    => 'menu_order ID',
				'id'         => $post ? $post->ID : 0,
				'itemtag'    => $html5 ? 'figure'     : 'dl',
				'icontag'    => $html5 ? 'div'        : 'dt',
				'captiontag' => $html5 ? 'figcaption' : 'dd',
				'size'       => 'thumbnail',
				'include'    => '',
				'exclude'    => '',
				'link'       => ''
			), $attr, 'gallery' );

			$id = intval( $atts['id'] );

			if ( ! empty( $atts['include'] ) ) {
				$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

				$attachments = array();
				foreach ( $_attachments as $key => $val ) {
					$attachments[$val->ID] = $_attachments[$key];
				}
			} elseif ( ! empty( $atts['exclude'] ) ) {
				$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
			} else {
				$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
			}

			if ( empty( $attachments ) ) {
				return '';
			}

			if ( is_feed() ) {
				$output = "\n";
				foreach ( $attachments as $att_id => $attachment ) {
					$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
				}
				return $output;
			}

			$itemtag = tag_escape( $atts['itemtag'] );
			$captiontag = tag_escape( $atts['captiontag'] );
			$icontag = tag_escape( $atts['icontag'] );
			$valid_tags = wp_kses_allowed_html( 'post' );
			if ( ! isset( $valid_tags[ $itemtag ] ) ) {
				$itemtag = 'dl';
			}
			if ( ! isset( $valid_tags[ $captiontag ] ) ) {
				$captiontag = 'dd';
			}
			if ( ! isset( $valid_tags[ $icontag ] ) ) {
				$icontag = 'dt';
			}

			$float = is_rtl() ? 'right' : 'left';

			$selector = "gallery-{$instance}";

			$size_class = sanitize_html_class( $atts['size'] );

			// Build images for slider
			$output	= '<div class="owl-carousel owl-theme">' . "\n";

			// Build thumbs for controls
			$thumbs_output	= '<div class="gallery">';

			foreach ( $attachments as $id => $attachment ) {

				$attr			= ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
				// $image_output	= wp_get_attachment_image( $id, 'medium', false, $attr );
				$image_output	= wp_get_attachment_link( $id, 'medium', false, false, false, $attr );
				$thumb_output	= wp_get_attachment_link( $id, 'thumbnail', false, false, false, $attr );

				// add lazy loading to image
				$image_output	= str_replace( ' src=', ' class="owl-lazy" data-src=', $image_output );

				// add fancybox to gallery
				$image_output	= str_replace( '<a href=', '<a data-fancybox="gallery" data-type="image" href=', $image_output );

				$output	.= $image_output . "\n";

				$image_meta  = wp_get_attachment_metadata( $id );

				// if the image is a gif, we want the markup for a ribbon to highlight to the user
				if ( substr_compare( $image_meta['file'], '.gif', -4 ) === 0 ) {
					$image_output	= str_replace( '<img ', '<div class="ribbon"><div class="text">GIF</div></div><img ', $image_output );
				}

				// $thumbs_output

			}

			$output	.= '</div>';

			$thumbs_output	.= '</div>';

			return $output;



			$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

			$output = $gallery_div;

			$i = 0;
			foreach ( $attachments as $id => $attachment ) {

				$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
				if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
					$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
				} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
					$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
				} else {
					$image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
				}
				$image_meta  = wp_get_attachment_metadata( $id );

				// if the image is a gif, we want the markup for a ribbon to highlight to the user
				if ( substr_compare( $image_meta['file'], '.gif', -4 ) === 0 ) {
					$image_output	= str_replace( '<img ', '<div class="ribbon"><div class="text">GIF</div></div><img ', $image_output );
				}

				// add fancybox to gallery
				$image_output	= str_replace( '<a href=', '<a data-fancybox="gallery" href=', $image_output );

				$orientation = '';
				if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
					$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
				}
				$output .= "<{$itemtag} class='gallery-item'>";
				$output .= "
					<{$icontag} class='gallery-icon {$orientation}'>
						$image_output
					</{$icontag}>";
				if ( $captiontag && trim($attachment->post_excerpt) ) {
					$output .= "
						<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
						" . wptexturize($attachment->post_excerpt) . "
						</{$captiontag}>";
				}
				$output .= "</{$itemtag}>";
				if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
					$output .= '<br style="clear: both" />';
				}
			}

			$output .= "
				</div>\n";

			return $output;
		}

	}

