<?php

class Theme_Setup {

	public static $instance = false;

	public static $is_phone = null;

	public static $is_mobile = null;

	public static $is_tablet = null;

	public static $used_posts = array();

	public function __construct() {
		$this->_setup_hooks();
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _setup_hooks() {

		// on theme activation actions
		add_action( 'switch_theme', array( $this, 'set_image_sizes' ) );

		// front-end actions
		add_action( 'pre_get_posts', array( $this, 'alter_queries' ) );
		// add_action( 'wp_head', array( $this, 'inline_style' ), 0 );
		// add_action( 'wp_head', array( $this, 'add_fonts_asynchronously' ), 0 );
		add_action( 'wp_head', array( $this, 'generate_favicon' ) );
		add_action( 'wp_head', array( $this, 'generate_google_analytics' ) );
		// add_action( 'wp_head', array( $this, 'generate_og_image' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ), 100 );

		// custom front-end actions
		add_action( 'head_begin_hook', array( $this, 'head_begin_settings' ) );
		add_action( 'head_end_hook', array( $this, 'head_end_settings' ) );
		add_action( 'body_begin_hook', array( $this, 'body_begin_settings' ) );
		add_action( 'body_end_hook', array( $this, 'body_end_settings' ) );

		// front-end filters
		add_filter( 'script_loader_tag', array( $this, 'do_script_loader_tag' ), 10, 3 );
		add_filter( 'excerpt_length', array( $this, 'custom_excerpt_length' ), 999 );
		// add_filter( 'post_gallery', array( $this, 'add_fancybox_gallery' ), 10, 3 );
		// add_filter( 'wp_title', array( $this, 'custom_titles' ), 10, 2 );
		add_filter( 'the_content', array( $this, 'filter_ptags_on_images' ) );
		add_filter( 'body_class', array( $this, 'add_slug_body_class' ) );

		// remove nasty features of WP
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		// add_filter( 'emoji_svg_url', '__return_false' );
		remove_action( 'wp_head', 'wp_resource_hints', 2 );

		// remove WP version number
		remove_action( 'wp_head', 'wp_generator' );
		add_filter( 'style_loader_src', array( $this, 'remove_wp_ver_css_js' ), 9999 );
		add_filter( 'script_loader_src', array( $this, 'remove_wp_ver_css_js' ), 9999 );

		// default image compression quality is 82, I personally want it at 90
		add_filter( 'jpeg_quality', function() { return 90; } );

	}

	/**
	 * Enqueue Assets
	 *
	 * Enqueues the necessary css and js files when the theme is loaded.
	 */
	public function enqueue_assets() {

		// remove the wp-embed script, not in use
		wp_deregister_script( 'wp-embed' );

		// general javascript
		wp_deregister_script( 'jquery' );
		// wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', true );

		/** Theme files at end so they get priority in cascade */
		// if ( defined('WP_ENV') && WP_ENV == 'testing' ) {
			// Only include this on dev/testing so that Browser Sync will work
			wp_enqueue_style( 'sandm', get_template_directory_uri() .'/css/style.min.css',array(), filemtime( get_template_directory() .'/css/style.min.css') );
		// }
		wp_enqueue_script( 'sandm', get_template_directory_uri() .'/js/scripts.min.js', array(), filemtime( get_template_directory() .'/js/scripts.min.js' ), true );

		wp_localize_script( 'sandm', 'search_post_data', array(
			'ajax_url'	=> admin_url( 'admin-ajax.php' )
		) );

	}

	/**
	 * Adds the Theme Settings "After <head> Tag" code through WP hook
	 */
	public function head_begin_settings() {

		/*$in_head_section = Triathlete_Theme_Settings::get_theme_option( 'in_head_section' );
		if ( !empty( $in_head_section ) ) {
			echo $in_head_section;
		}*/

	}

	/**
	 * Adds the Theme Settings "Before </head> Tag" code through WP hook
	 */
	public function head_end_settings() {

		/*$before_closing_head = Triathlete_Theme_Settings::get_theme_option( 'before_closing_head' );
		if ( !empty( $before_closing_head ) ) {
			echo $before_closing_head;
		}*/

	}

	/**
	 * Adds the Theme Settings "After <body> Tag" code through WP hook
	 */
	public function body_begin_settings() {

		/*$after_opening_body = Triathlete_Theme_Settings::get_theme_option( 'after_opening_body' );
		if ( !empty( $after_opening_body ) ) {
			echo $after_opening_body;
		}*/

	}

	/**
	 * Adds the Theme Settings "Before </body> Tag" code through WP hook
	 */
	public function body_end_settings() {

		/*$before_closing_body = Triathlete_Theme_Settings::get_theme_option( 'before_closing_body' );
		if ( !empty( $before_closing_body ) ) {
			echo $before_closing_body;
		}*/

	}

	/**
	 * Do Script Loader Tag
	 *
	 * Allows enqueued scripts to be loaded asynchronously, thus preventing the
	 * page from being blocked by js calls.
	 *
	 * @param  string $tag    The <script> tag for the enqueued script.
	 * @param  string $handle The script's registered handle.
	 * @param  string $src    The script's source URL.
	 *
	 * @return string The formatted HTML script tag of the given enqueued script.
	 */
	public function do_script_loader_tag( $tag = null, $handle = null, $src = null ) {

		// the handles of the enqueued scripts we want to async
		$async_scripts = array( 'sandm' );

		if ( !in_array( $handle, $async_scripts ) ) {
			return $tag;
		}

		return str_replace( ' src', ' async="async" src', $tag );
	}

	/**
	 * Remove the paragraph tags wrapped around img tags in the content
	 *
	 * @param  string	$content	The content for the object as a string
	 * @return string				Altered content string without img tags wrapped in p tags
	 */
	public function filter_ptags_on_images( $content ) {

		$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);

		return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);

	}

	/**
	 * Add the slug of a page to the body_class
	 *
	 * @param	Array	$classes List of classes that will be outputed into the body class
	 */
	public function add_slug_body_class( $classes ) {

		$queried_object = get_queried_object();

		if ( isset( $queried_object->post_type ) && $queried_object->post_type == 'page' ) {
			$classes[] = $queried_object->post_type . '-' . $queried_object->post_name;
		}

		return $classes;
	}


	/**
	 * Alters our queries in case we forget to add these parameters to our queries
	 * @param  object $query the original query
	 */
	public function alter_queries( $query ) {

		// we don't want to show posts with passwords ever
		$query->set( 'has_password', false );
		// we ignore sticky posts, always ALWAYS AND AAAAAAAAAALWAYS
		$query->set( 'ignore_sticky_posts', true );

	}

	/**
	 * Adding Google fonts asynchronously because it blocks page loading if loaded old fashion way
	 */
	public function add_fonts_asynchronously() { ?>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/typeit/5.0.1/typeit.min.js"></script>

		<script>
			WebFontConfig = {
				// classes: false,
				// events: false,
				google: {
					families: ['Raleway:400,700', 'Open+Sans:400,600,700']
				},
				custom: {
					families: ['League Gothic']
				},
				fontactive: function(familyName, fvd) {

					if ( familyName == 'League Gothic' ) {
						console.log(familyName);
						// Typeit
						var headers = [].slice.call(document.querySelectorAll('.home h2'));

						headers.forEach( function (element) {
							element.style.display	= 'inline-block';

						    new TypeIt(element, {
						        speed: 50,
						        autoStart: false,
						        callback: function() {
						            element.classList.add('typeit-done');
						        }
						    });

						});

					} // end if

				}
			};

			(function(d) {
				var wf = d.createElement('script'),
					s = d.scripts[0];
				wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
				wf.async = true;
				s.parentNode.insertBefore(wf, s);
			})(document);
		</script>

		<?php
	}

	/**
	 * Generate Favicon
	 *
	 * Generates the markup for the favicon.
	 * Passed into wp_head.
	 */
	public function generate_favicon() {
		$template_uri = get_template_directory_uri(); ?>

		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $template_uri; ?>/images/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $template_uri; ?>/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $template_uri; ?>/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo $template_uri; ?>/images/favicon/site.webmanifest">
		<link rel="shortcut icon" href="<?php echo $template_uri; ?>/images/favicon/favicon.ico">
		<meta name="msapplication-config" content="<?php echo $template_uri; ?>/images/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

		<?php
	}

	/**
	 * Generates the Google Analytics code
	 */
	public function generate_google_analytics() {

		// Lets not add it if we are logged in or on dev/local testing
		if ( !is_user_logged_in() && defined('WP_ENV') && WP_ENV !== 'testing' ) { ?>

			<!-- Google Analytics -->
			<script>
				window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
				ga('create', 'UA-13018822-3', 'auto');

				ga('require', 'eventTracker');
				ga('require', 'maxScrollTracker');
				ga('require', 'outboundLinkTracker');
				ga('require', 'pageVisibilityTracker');

				ga('send', 'pageview');
			</script>
			<script async src='https://www.google-analytics.com/analytics.js'></script>
			<!-- End Google Analytics -->

		<?php }

	}

	/**
	 * Display the styles file inline for better Google speed test
	 * WP Action has priority 0 so that it gets read ASAP
	 *
	 * NOTE: avoid this on dev/testing so that Browser Sync CSS injection can work
	 */
	public function inline_style() {

		if ( defined('WP_ENV') && WP_ENV !== 'testing' ) {

			$source = get_template_directory() .'/css/style.min.css';

			if ( file_exists($source) ) {

				$contents	= file_get_contents( $source );

				// strip the comments and source map
				$contents	= preg_replace("#/\\*.*?\\*/#s", '', $contents);


				// add full path to urls
				$contents	= str_replace( 'url(', 'url('. get_template_directory_uri() .'/css/', $contents );

				echo '<style>'."\n\t\t";
				echo '/* STYLE HERE FOR PAGESPEED SCORE (I do not like this but...yeah, it works) */'."\n\t\t";
				echo $contents."\n\t\t";
				echo '</style>'."\n";
			}

		}

	}

	/*
	 * Generates og:image meta tag for images in social sharing
	 */
	public function generate_og_image() {

		// Check if the WPSEO function that will handle this exists (plugin is on)
		// If it is, let that handle the og:image meta tag instead of here (don't want double meta tags)
		if ( class_exists('WPSEO_OpenGraph_Image') ) return;

		$og_image	= Triathlete_Theme_Settings::get_theme_option( 'og_image_default' );

		if ( is_single() || is_page() ) {

			$post_id	= get_the_ID();
			$has_thumb	= has_post_thumbnail( $post_id );

			$og_image	= $has_thumb ? wp_get_attachment_url( get_post_thumbnail_id( $post_id ) ) : $og_image;

		}

		if ( $og_image != '' && !is_single() ) {

			echo '<meta property="og:image" content="'. $og_image .'">' . "\n";
			echo '<meta property="twitter:image" content="'. $og_image .'">' . "\n";

		}

	}

	/**
	 * Remove the WP version param from any scripts
	 * @param  string $src full source path
	 * @return string      (un)altered source path
	 */
	public function remove_wp_ver_css_js( $src ) {
		if ( !is_user_logged_in() && strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) ) {
			// Little easter egg for our ex-sysadmin
			$src	= remove_query_arg( 'ver', $src );
			$src	= add_query_arg( 'bbreve', SM_VER, $src );
		}
		return $src;
	}

	/**
	 * Set Theme Options
	 *
	 * Configures the necessary WordPress theme options once the theme is activated.
	 */
	public static function do_after_theme_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );

		// Allow WordPress to generate the title tag dynamically.
		add_theme_support( 'title-tag' );

		// thumbnail image dimensions
		// set_post_thumbnail_size( 150, 150, true );

		// longform image dimensions
		// add_image_size( 'longform-featured', 1170, 425, false );

		// Add support for cropping default WordPress medium images -
		add_image_size( 'medium', 500, 800 );
		add_image_size( 'cropped-thumb', 357, 194, array( 'center', 'top' ) );

		// This theme uses wp_nav_menu() in various locations.
		/*register_nav_menus( array(
			'main-navigation'	=> __( 'Main Navigation', 'sandm' ),
		) );*/

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array( 'search-form', 'gallery', 'caption' ) );

		/**
		 * prevent wp_site_icon() from adding favicons to the wp_head
		 * we already have a function that handles that
		 * must add an image to the customizer so that AMP can work properly
		 */
		// remove_action( 'wp_head', 'wp_site_icon', 99 );

		add_filter( 'wp_calculate_image_srcset_meta', '__return_null' );

		// disable REST API / oEmbed
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

	}

	/**
	 * Set theme image sizes
	 *
	 * Ran with the 'switch_theme' action so that the query does not run EVERY page load
	 */
	public static function set_image_sizes() {

		// override the media settings size for thumbnails
		update_option( 'thumbnail_size_w', 150 );
		update_option( 'thumbnail_size_h', 150 );

		// medium image dimensions
		update_option( 'medium_size_w', 357 );
		update_option( 'medium_size_h', 194 );

		// large image dimensions
		update_option( 'large_size_w', 900 );
		update_option( 'large_size_h', 1024 );

	}

	/**
	 * Generates the image data of the existing image like "wp_get_attachment_image_src"
	 * BUT also generates the image sizes if needed
	 *
	 * @param  int		$img_id ID of the image
	 * @param  string	$size   Requested size
	 * @return array			Holds the URL, width, and height of image. False if the passed $size does not exist or other securities.
	 */
	public static function get_image_data( $img_id = null, $size = 'post-thumbnail' ) {
		// OK THIS IS GOING TO GET WEIRD UP IN HERE
		// We are trying to only generate images we actually need to save server space. Don't be selfish.
		// 1. Check the width/height of the returned image is the size the theme actually needs (thumbnail was before)
		// 2. If doesn't exist, regenerate and update/save the new data of the image

		// Check that the requested size exists, or abort
		$img_sizes	= get_intermediate_image_sizes();

		if ( !in_array( $size, $img_sizes ) )
			return false;

		if ( $img_id != null && $size != 'full' ) {

			$img_data			= wp_get_attachment_metadata( $img_id );
			$img_settings		= self::get_theme_image_size( $size );

			if ( !empty( $img_data['sizes'] ) && array_key_exists( $size, $img_data['sizes'] ) ) {

				$requested_img_data	= $img_data['sizes'][$size];

			} else {
				return image_downsize( $img_id, $size );
			}

			// If the image exists with the theme settings' dimensions, return the normal data array
			if (
				!empty( $requested_img_data )
				&& ( $requested_img_data['width'] == $img_settings['width'] )
				&& ( $requested_img_data['height'] == $img_settings['height'] )
			) {

				return wp_get_attachment_image_src( $img_id, $size );

			}

			// Generate the new image size with the image settings
			$resized = image_make_intermediate_size( get_attached_file($img_id), $img_settings['width'], $img_settings['height'], $img_settings['crop'] );
			if ( !$resized )
				return false;

			// Save image meta, or WP can't see that the thumb exists now
			$img_data['sizes'][$size] = $resized;

			wp_update_attachment_metadata( $img_id, $img_data );

			// Return the array for displaying the resized image
			$att_url = wp_get_attachment_url($img_id);

			return array( dirname($att_url) . '/' . $resized['file'], $resized['width'], $resized['height'], true );

		} else {

			return false;

		}

	}

	/**
	 * Limit the excerpt length by words
	 *
	 * @param  int $length Default number of words for excerpt
	 *
	 * @return int         New number of words to return as the excerpt
	 */
	public static function custom_excerpt_length( $length ) {
		return 25;
	}

	/**
	 * Adding the required attribute for Fancybox
	 *
	 * @param string $output	The gallery output. Default empty.
	 * @param array  $attr		Attributes of the gallery shortcode.
	 * @param int    $instance	Unique numeric ID of this gallery shortcode instance.
	 *
	 * @return					The modded markup
	 */
	public static function add_fancybox_gallery( $html, $attr, $instance ) {

		// Remove the filter to prevent infinite loop.
		/*remove_filter( 'post_gallery', array( self::$instance, 'add_fancybox_gallery' ), 10, 3 );

		$html	= gallery_shortcode( $attr );
		$html	= str_replace( '<a href', '<a data-fancybox="gallery" href', $html );

		return $html;*/

		add_filter( 'wp_get_attachment_link', array( self::$instance, 'add_fancybox_gallery_final' ) );

	}

	public static function add_fancybox_gallery_final( $link ) {
		return str_replace( '<a href=', '<a data-fancybox="gallery" href=', $link );
	}

	/**
	 * Get size information for all currently-registered image sizes.
	 *
	 * @global $_wp_additional_image_sizes
	 * @uses   get_intermediate_image_sizes()
	 * @return array $sizes Data for all currently-registered image sizes.
	 */
	protected static function get_theme_image_sizes() {
		global $_wp_additional_image_sizes;

		$sizes = array();

		foreach ( get_intermediate_image_sizes() as $_size ) {

			if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {

				$sizes[ $_size ]['width']	= get_option( "{$_size}_size_w" );
				$sizes[ $_size ]['height']	= get_option( "{$_size}_size_h" );
				$sizes[ $_size ]['crop']	= (bool) get_option( "{$_size}_crop" );

			} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

				$sizes[ $_size ] = array(
					'width'		=> $_wp_additional_image_sizes[ $_size ]['width'],
					'height'	=> $_wp_additional_image_sizes[ $_size ]['height'],
					'crop'		=> $_wp_additional_image_sizes[ $_size ]['crop'],
				);

			}

		}

		return $sizes;
	}

	/**
	 * Get size information for a specific image size.
	 *
	 * @uses   get_theme_image_size()
	 * @param  string $size The image size for which to retrieve data.
	 * @return bool|array $size Size data about an image size or false if the size doesn't exist.
	 */
	protected static function get_theme_image_size( $size ) {
		$sizes	= self::get_theme_image_sizes();

		if ( isset( $sizes[ $size ] ) )
			return $sizes[ $size ];

		return false;
	}

}
