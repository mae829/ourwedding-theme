<?php
/**
 * Class for Triathlete's Comments
 * Since we don't have comments in Tri, we get rid of all instances of them through out the admin area
 * Also remove from front end so it doesn't make any queries to any of the WPcomment features
 *
 * Note: To add them back, just remove the call to this class in the functions.php file
 * Credit for these functions goes to the following:
 * 		https://www.dfactory.eu/turn-off-disable-comments/
 * 		https://github.com/solarissmoke/disable-comments
 */
class Bleu_Comments {

	public static $instance = false;

	public function __construct() {
		$this->_add_actions();
	}

	public function disable_rc_widget() {
		unregister_widget( 'WP_Widget_Recent_Comments' );
	}

	/*
	 * Remove the X-Pingback HTTP header
	 */
	public function filter_wp_headers( $headers ) {
		unset( $headers['X-Pingback'] );
		return $headers;
	}

	/*
	 * Issue a 403 for all comment feed requests.
	 * Bad thing, big nono for Google'S crawler. Commented out in _add_actions
	 */
	public function filter_query() {
		if ( is_comment_feed() ) {
			wp_die( __( 'Comments are closed.', 'triathlete' ), '', array( 'response' => 403 ) );
		}
	}

	/*
	 * Remove comment links from the admin bar.
	 */
	public function filter_admin_bar() {
		if ( is_admin_bar_showing() ) {
			// Remove comments links from admin bar
			remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );	// WP 3.3
		}
	}

	/*
	 * Disable support for comments and trackbacks in post types.
	 */
	public function init_wploaded_filters() {

		$post_types = get_post_types();

		foreach ( $post_types as $post_type ) {
			if ( post_type_supports( $post_type, 'comments' ) ) {
				remove_post_type_support( $post_type, 'comments' );
				remove_post_type_support( $post_type, 'trackbacks' );
			}
		}

		// add_filter( 'comments_array', array( $this, 'filter_existing_comments' ), 20, 2 );
		add_filter( 'comments_open', '__return_false', 20, 2 );
		add_filter( 'pings_open','__return_false', 20, 2 );

	}

	// Close comments on the front-end
	public function disable_comments_status() {
		return false;
	}

	// Hide existing comments
	public function disable_comments_hide_existing_comments($comments) {
		$comments = array();
		return $comments;
	}

	// Remove comments page in menu
	public function disable_comments_admin_menu() {
		remove_menu_page( 'edit-comments.php' );
		remove_submenu_page( 'options-general.php', 'options-discussion.php' );
	}

	// Redirect any user trying to access comments page
	public function disable_comments_admin_menu_redirect() {
		global $pagenow;
		if ( in_array( $pagenow, array( 'edit-comments.php', 'comment.php', 'options-discussion.php' ) ) ) {
			wp_redirect( admin_url() ); exit;
		}
	}

	// Remove comments metabox from dashboard
	public function disable_comments_dashboard() {
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	}

	// Remove comments links from admin bar
	public function disable_comments_admin_bar() {
		if (is_admin_bar_showing()) {
			remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
		}
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if( !self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _add_actions() {
		/*
		UNUSED ACTIONS/FILTERS FROM https://www.dfactory.eu/turn-off-disable-comments/



		add_filter( 'comments_open', array( $this, 'disable_comments_status' ), 20, 2);

		add_filter( 'pings_open', array( $this, 'disable_comments_status' ), 20, 2);

		add_filter( 'comments_array', array( $this, 'disable_comments_hide_existing_comments' ), 10, 2);

		add_action( 'init', array( $this, 'disable_comments_admin_bar' ) );*/

		// add_action( 'admin_init', array( $this, 'disable_comments_admin_menu_redirect' ) );
		add_action( 'admin_init', array( $this, 'disable_comments_dashboard' ) );
		// add_action( 'admin_menu', array( $this, 'disable_comments_admin_menu' ) );

		add_action( 'widgets_init', array( $this, 'disable_rc_widget' ) );

		add_filter( 'wp_headers', array( $this, 'filter_wp_headers' ) );

		// add_action( 'template_redirect', array( $this, 'filter_query' ), 9 );

		add_action( 'template_redirect', array( $this, 'filter_admin_bar' ) );

		// remove the comments feeds
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		add_filter( 'feed_links_show_comments_feed', '__return_false' );

		add_action( 'wp_loaded', array( $this, 'init_wploaded_filters' ) );

	}

}

Bleu_Comments::singleton();
