<?php
	/**
	 * Class for listing all the AJAX and set up of invitees search and RSVP
	 */
	class Invitees {

		public static $instance = false;

		public function __construct() {
			$this->_setup_hooks();
		}

		/**
		 * Singleton
		 *
		 * Returns a single instance of the current class.
		 */
		public static function singleton() {

			if ( !self::$instance )
				self::$instance = new self();

			return self::$instance;

		}

		/**
		 * Set up hooks
		 *
		 * Defines all the WordPress actions and filters used by this class.
		 */
		protected function _setup_hooks() {

			// ajax calls
			add_action( 'wp_ajax_nopriv_search_post_data', array( $this, 'search_post_data_callback' ) );
			add_action( 'wp_ajax_search_post_data', array( $this, 'search_post_data_callback' ) );

		}

		public function search_post_data_callback(){
			global $post;
			ob_start();

			if ( !isset( $_POST['search_nonce'] )
				|| (
					!wp_verify_nonce( $_POST['search_nonce'], 'invitee_search' )
					&& !wp_verify_nonce( $_POST['search_nonce'], 'invitee_rsvp' )
					&& !wp_verify_nonce( $_POST['search_nonce'], 'invitee_submission' )
				)
			) {
				wp_send_json_error( 'Security failed. Please try again.' );
			} else {

				global $wpdb;

				if ( wp_verify_nonce( $_POST['search_nonce'], 'invitee_search' )  ) {

					$first_name	= sanitize_text_field( $_POST['first_name'] );
					$last_name	= sanitize_text_field( $_POST['last_name'] );

					// Build the SQL query string
					$search_query	= "SELECT ID, first_name, last_name, plusone, plusone_first_name, plusone_last_name FROM {$wpdb->prefix}invitees WHERE";

					if ( !empty( $first_name ) ) {
						$search_query	= $search_query . ' (first_name LIKE "%'. $first_name . '%" OR plusone_first_name LIKE "%'. $first_name .'%")';
					}

					if ( !empty( $first_name ) && !empty( $last_name ) ) {
						$search_query	= $search_query .' OR';
					}

					if ( !empty( $last_name ) ) {
						$search_query	= $search_query . ' (last_name LIKE "'. $last_name . '%" OR plusone_last_name LIKE "'. $last_name .'%")';
					}

					$search_query	= $search_query .' AND rsvpd IS NULL';

					$results = $wpdb->get_results( $search_query );

					if ( empty( $results ) ) {
						wp_send_json_error( 'Sorry, could not find an invitee with that name or you have already RSVP\'d. Please try again or contact Mikey.' );
					}

					wp_send_json_success( $results );

				} elseif ( wp_verify_nonce( $_POST['search_nonce'], 'invitee_rsvp' ) ) {

					// Check if they are attending, submit to DB if they are/are not
					if ( empty( $_POST['response'] ) || empty( $_POST['invitee_ID'] ) ) {
						wp_send_json_error( 'Data is missing in your request. What are you doing?' );
					} else {

						$invitee_ID = $_POST['invitee_ID'];
						$response	= $_POST['response'];

						if ( $response === 'no' ) {
							// Update RSVP status and return message
							$result = $wpdb->update(
								$wpdb->prefix .'invitees',
								array( 'rsvpd'=> 0 ),
								array( 'ID' => $invitee_ID ),
								array( '%d' )
							);

							if ( !$response ) {
								wp_send_json_error( 'Sorry, something went wrong with your submission! Please try again or contact Mikey.' );
							} else {
								wp_send_json_success( 'Thank you, your response has been submitted.' );
							}

						} elseif ( $response == 'yes' ) {

							// Fetch their data to ask about their RSVP (their info and plus one info if any)
							$search_query	= "SELECT * FROM {$wpdb->prefix}invitees WHERE ID = ". $invitee_ID;

							$result = $wpdb->get_row( $search_query );

							if ( NULL !== $result ) {
								wp_send_json_success( $result );
							} else {
								wp_send_json_error( 'Error selecting your invite information. Please try again.' );
							}

						} else {
							wp_send_json_error( 'Response answer was weird. What the fuck are you trying to do?' );
						}

					}


				} elseif ( wp_verify_nonce( $_POST['search_nonce'], 'invitee_submission' ) ) {

					$invitee_data	= $_POST['invitee_data'];
					$invitee_ID		= $invitee_data['invitee-id'];

					// Make sure the email submitted is actually an email
					if ( !is_email( $invitee_data['email'] ) ) {
						wp_send_json_error( 'The email you submitted is not valid. Please try again.' );
					}

					// if they had a Plus One, make sure it's a digit, not "on"
					if ( isset( $invitee_data['plusone'] ) && !empty( $invitee_data['plusone_attending'] ) ) {
						$invitee_data['plusone_attending'] = 1;
					} elseif ( isset( $invitee_data['plusone'] ) ) {
						$invitee_data['plusone_attending'] = 0;
					}

					// Clean up for what we need
					unset( $invitee_data['plusone'] );
					unset( $invitee_data['invitee-id'] );
					unset( $invitee_data['search_nonce'] );
					unset( $invitee_data['_wp_http_referer'] );

					// add a value for the column RSVPD so that they cannot search and change their choices
					$invitee_data['rsvpd'] = 1;

					$result = $wpdb->update(
						$wpdb->prefix .'invitees',
						$invitee_data,
						array( 'ID' => $invitee_ID )
					);

					// if there was no rows updated or some other error, return an error
					if ( !$result ) {
						wp_send_json_error( 'Error updating your submitted data. Please try again.' );
					}

					wp_send_json_success( 'Thank you for RSVP\'ing. We can\'t wait to celebrate with you!' );

				} else {
					wp_send_json_error( 'Action request error. Please try again.' );
				}

			}

			$output = ob_get_clean();

			echo $output;

			die();
		}

	}

