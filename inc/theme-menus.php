<?php

class Theme_Menus {

	public static $instance = false;

	public function __construct() {
		$this->_setup_hooks();
	}

	/**
	 * Singleton
	 *
	 * Returns a single instance of the current class.
	 */
	public static function singleton() {

		if ( !self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Add Actions
	 *
	 * Defines all the WordPress actions and filters used by this theme.
	 */
	protected function _setup_hooks() {

		//show the menu from cache
		add_filter( 'pre_wp_nav_menu', array( $this, 'pre_wp_nav_menu' ), 10, 2 );

		//store the menu in cache
		add_filter( 'wp_nav_menu', array( $this, 'wp_nav_menu' ), 10, 2);

		add_action( 'wp_update_nav_menu', array( $this, 'delete_main_menu_transient' ), 10, 2 );

	}

	public function pre_wp_nav_menu( $nav_menu, $args ) {

		// set a unique name for our menu based on the page we are viewing (so active page is highlighted properly)
		$menu_transient_name	= 'menu-'. $args->theme_location .'-'. md5( serialize( get_queried_object() ) );

		if ( false !== $menu = get_transient( $menu_transient_name ) ) {
			// return the already cached menu for display
			return $menu .'<!-- from cache -->';
		}

		return $nav_menu;

	}

	public function wp_nav_menu( $nav_menu, $args ) {

		// set a unique name for our menu based on the page we are viewing (so active page is highlighted properly)
		$menu_transient_name	= 'menu-'. $args->theme_location .'-'. md5( serialize( get_queried_object() ) );

		if ( false === get_transient( $menu_transient_name ) ) {
			// save the generated menu into the transient since the transient was false
			set_transient( $menu_transient_name, $nav_menu );
		}

		return $nav_menu .'<!-- NOT from cache -->';

	}

	/**
	 * Determines whether or not to delete a transient of the menu when "wp_update_nav_menu" is fired.
	 * Menus are cached with no expiration until a menu is saved again
	 *
	 * @param int	$menu_ID	ID of the updated menu
	 * @param array	$menu_data	An array of menu data
	 */
	public function delete_main_menu_transient( $menu_ID = null, $menu_data = array() ) {

		$locations = get_nav_menu_locations();

		// if the menu being saved is one of our registered_nav menus, delete it's transients
		if ( false !== $saved_menu = array_search( $menu_ID, $locations ) ) {
			$this->delete_transients( 'menu-' . $saved_menu );
		}

	}

	/**
	 * Delete all transients with a key prefix.
	 *
	 * @param string $prefix The key prefix.
	 */
	function delete_transients( $prefix ) {
		$this->delete_transients_from_keys( $this->search_db_for_transients_by_prefix( $prefix ) );
	}

	/**
	 * Searches the database for transients stored there that match a specific prefix.
	 *
	 * @param  string $prefix Prefix to search for.
	 * @return array|bool     Nested array response for wpdb->get_results or false on failure.
	 */
	function search_db_for_transients_by_prefix( $prefix ) {

		global $wpdb;

		// Add our prefix after concating our prefix with the _transient prefix
		$prefix = $wpdb->esc_like( '_transient_' . $prefix );

		// Build up our SQL query
		$sql = "SELECT `option_name` FROM $wpdb->options WHERE `option_name` LIKE '%s'";

		// Execute our query
		$transients = $wpdb->get_results( $wpdb->prepare( $sql, $prefix . '%' ), ARRAY_A );

		// If it looks good, pass it back
		if ( $transients && ! is_wp_error( $transients ) ) {
			return $transients;
		}

		// Otherwise return false
		return false;
	}

	/**
	 * Expects a passed in multidimensional array of transient keys.
	 *
	 * array(
	 *     array( 'option_name' => '_transient_blah_blah' ),
	 *     array( 'option_name' => 'transient_another_one' ),
	 * )
	 *
	 * Can also pass in an array of transient names.
	 *
	 * @param  array|string $transients  Nested array of transients, keyed by option_name,
	 *                                   or array of names of transients.
	 * @return array|bool                Count of total vs deleted or false on failure.
	 */
	function delete_transients_from_keys( $transients ) {

		if ( ! isset( $transients ) ) {
			return false;
		}

		// If we get a string key passed in, might as well use it correctly
		if ( is_string( $transients ) ) {
			$transients = array( array( 'option_name' => $transients ) );
		}

		// If its not an array, we can't do anything
		if ( ! is_array( $transients ) ) {
			return false;
		}

		$results = array();

		// Loop through our transients
		foreach ( $transients as $transient ) {

			if ( is_array( $transient ) ) {

				// If we have an array, grab the first element
				$transient = current( $transient );
			}

			// Remove that sucker
			$results[ $transient ] = delete_transient( str_replace( '_transient_', '', $transient ) );
		}

		// Return an array of total number, and number deleted
		return array(
			'total'   => count( $results ),
			'deleted' => array_sum( $results ),
		);
	}

}
